<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();
        return view('event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventTopic = EventTopic::all();
        $eventCategory = EventCategory::all();
        return view('event.create',compact('eventTopic','eventCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'image' => 'required',
            'description' => 'required',
            'organisername' => 'required',
            'organiserdescription' => 'required',
            'eventcategoryid' => 'required',
            'eventtopicid' => 'required',
        ]);
        Event::create($request->all());

        return redirect()->route('event.index')

            ->with('success', 'Event created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $where = array('id' => $id);
        $data['event_info'] = Event::where($where)->first();
        return view('event.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $event_info = Event::where($where)->first();
        $eventcategory_info = EventCategory::all();
        $eventtopic_info = EventTopic::all();
        return view('event.edit', compact('event_info','eventcategory_info','eventtopic_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'image' => 'required',
            'description' => 'required',
            'organisername' => 'required',
            'organiserdescription' => 'required',
            'eventcategoryid' => 'required',
            'eventtopicid' => 'required',
        ]);
         
        $update = ['name' => $request->name];
        $updatelocation = ['location' => $request->location];
        $updatestarts = ['starts' => $request->starts];
        $updateends = ['ends' => $request->ends];
        $updateimage = ['image' => $request->image];
        $updatedescription = ['description' => $request->description];
        $updateorganisername = ['organisername' => $request->organisername];
        $updatorganiserdescription = ['organiserdescription' => $request->organiserdescription];
        $updateeventcategoryid = ['eventcategoryid' => $request->eventcategoryid];
        $updateeventtopicid = ['eventtopicid' => $request->eventtopicid];
        Event::where('id',$id)->update($update);
        Event::where('id',$id)->update($updatelocation);
        Event::where('id',$id)->update($updatestarts);
        Event::where('id',$id)->update($updateends);
        Event::where('id',$id)->update($updateimage);
        Event::where('id',$id)->update($updatedescription);
        Event::where('id',$id)->update($updateorganisername);
        Event::where('id',$id)->update($updatorganiserdescription);
        Event::where('id',$id)->update($updateeventcategoryid);
        Event::where('id',$id)->update($updateeventtopicid);
        return redirect()->route('event.index')

            ->with('success', 'event updated successfully.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        Event::where('id',$id)->delete();
        return redirect()->route('event.index')

            ->with('success', 'Event deleted successfully.');
    }
}
