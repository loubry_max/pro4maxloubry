<?php

namespace App\Http\Controllers;

use App\EventCategory;
use Illuminate\Http\Request;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventCategory = EventCategory::all();
        return view('eventcategory.index', compact('eventCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventCategory::create($request->all());
        return redirect()->route('eventcategory.index')

            ->with('success', 'EventCategory created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $where = array('id' => $id);
        $data['eventcategory_info'] = EventCategory::where($where)->first();
        return view('eventcategory.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $data['eventcategory_info'] = EventCategory::where($where)->first();
        return view('eventcategory.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
         
        $update = ['name' => $request->name];
        EventCategory::where('id',$id)->update($update);
   
        return redirect()->route('eventcategory.index')

            ->with('success', 'EventCategory updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EventCategory::where('id',$id)->delete();
        return redirect()->route('eventcategory.index')

            ->with('success', 'EventCategory deleted successfully.');
    }
}