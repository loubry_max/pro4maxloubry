<?php
/*
https://www.positronx.io/php-laravel-crud-operations-mysql-tutorial/
https://www.itsolutionstuff.com/post/laravel-7-crud-example-laravel-7-tutorial-for-beginnersexample.html
*/
namespace App\Http\Controllers;

use App\EventTopic;
use Illuminate\Http\Request;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventTopic = EventTopic::all();
        return view('eventtopic.index', compact('eventTopic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventTopic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventTopic::create($request->all());
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $where = array('id' => $id);
        $data['eventtopic_info'] = EventTopic::where($where)->first();
        return view('eventtopic.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $data['eventtopic_info'] = EventTopic::where($where)->first();
        return view('eventtopic.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
         
        $update = ['name' => $request->name];
        EventTopic::where('id',$id)->update($update);
   
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EventTopic::where('id',$id)->delete();
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic deleted successfully.');
    }
}