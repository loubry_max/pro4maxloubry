<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name',
        'location',
        'starts',
        'ends',
        'image',
        'description',
        'organisername',
        'organiserdescription',
        'eventcategoryid',
        'eventtopicid',
    ];

}
