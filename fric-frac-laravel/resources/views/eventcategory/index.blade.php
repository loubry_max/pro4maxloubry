@extends('layout')
@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <h2>Event Category</h2>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="100px">Action</th>
            <th width="100px">Action</th>
            <th width="100px">Action</th>
        </tr>
        @foreach ($eventCategory as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td><a href="{{ route('eventcategory.show',$item->id)}}" class="btn btn-info">Show</a></td>
            <td><a href="{{ route('eventcategory.edit',$item->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
            <form action="{{ route('eventcategory.destroy', $item->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button>
            </td>
            </form>
        </tr>
        @endforeach
    </table>
    <div class="pull-left">
    <a href="{{ route('admin.index')}}" class="btn btn-info">Back to Admin</a>
    </div>
    <div class="pull-right">
    <a class="btn btn-success" href="{{ route('eventcategory.create') }}"> Create Event Category</a>
    </div>
 @endsection