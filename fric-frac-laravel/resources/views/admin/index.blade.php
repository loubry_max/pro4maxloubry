@extends('layout')
@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
                <a href="{{ route('eventcategory.index')}}" class="btn btn-info">EventCategory</a>
                <a href="{{ route('eventtopic.index')}}" class="btn btn-info">EventTopic</a>
                <a href="{{ route('event.index')}}" class="btn btn-info">Event</a>
            </div>
        </div>
    </div>