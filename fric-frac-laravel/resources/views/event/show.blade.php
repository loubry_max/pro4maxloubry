@extends('layout')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Show Event</h2>
       </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form >
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $event_info->name }}
            </div>
            <div class="form-group">
                <strong>Location:</strong>
                {{ $event_info->location }}
            </div>
            <div class="form-group">
                <strong>Starts:</strong>
                {{ $event_info->starts }}
            </div>
            <div class="form-group">
                <strong>Ends:</strong>
                {{ $event_info->ends }}
            </div>
            <div class="form-group">
                <strong>Image:</strong>
                {{ $event_info->image }}
            </div>
            <div class="form-group">
                <strong>Description:</strong>
                {{ $event_info->description }}
            </div>
            <div class="form-group">
                <strong>Organiser Name:</strong>
                {{ $event_info->organisername }}
            </div>
            <div class="form-group">
                <strong>Organiser Description:</strong>
                {{ $event_info->organiserdescription }}
            </div>
            <div class="form-group">
                <strong>EventCategoryId:</strong>
                {{ $event_info->eventcategoryid }}
            </div>
            <div class="form-group">
                <strong>EventTopicId:</strong>
                {{ $event_info->eventtopicid }}
            </div>
        </div>
    </div>
</form>
@endsection