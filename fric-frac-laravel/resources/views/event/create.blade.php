@extends('layout')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Event</h2>
       </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('event.store') }}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
            <div class="form-group">
                <strong>Location:</strong>
                <input type="text" name="location" class="form-control" placeholder="Location">
            </div>
            <div class="form-group">
                <strong>Starts:</strong>
                <input type="datetime-local" name="starts" class="form-control">
            </div>
            <div class="form-group">
                <strong>Ends:</strong>
                <input type="datetime-local" name="ends" class="form-control">
            </div>
            <div class="form-group">
                <strong>Image:</strong>
                <input type="text" name="image" class="form-control">
            </div>
            <div class="form-group">
                <strong>Description:</strong>
                <input type="text" name="description" class="form-control" placeholder="Description">
            </div>
            <div class="form-group">
                <strong>Organiser Name:</strong>
                <input type="text" name="organisername" class="form-control" placeholder="Organiser Name">
            </div>
            <div class="form-group">
                <strong>Organiser Description:</strong>
                <input type="text" name="organiserdescription" class="form-control" placeholder="Organiser Description">
            </div>
            <div class="form-group">
            <label for="eventcategoryid">
                <strong>EventCategoryId:</strong>
            </label>
                <select name="eventcategoryid" id="eventcategoryid" class="form-control">
                @foreach($eventCategory as $eventCategory)
                    <option value="{{ $eventCategory->id }}">{{ $eventCategory->id }}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <strong>EventTopicId:</strong>
                <select name="eventtopicid" id="eventtopicid" class="form-control">
                @foreach($eventTopic as $eventTopic)
                    <option value="{{ $eventTopic->id }}">{{ $eventTopic->id }}</option>
                @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection