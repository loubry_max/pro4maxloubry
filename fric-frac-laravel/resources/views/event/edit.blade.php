@extends('layout')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Event</h2>
       </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('event.update', $event_info->id) }}" method="POST" name="update_Event">
    @csrf
    @method('PATCH')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" value="{{ $event_info->name }}">
            </div>
            <div class="form-group">
                <strong>Location:</strong>
                <input type="text" name="location" class="form-control" value="{{ $event_info->location }}">
            </div>
            <div class="form-group">
                <strong>Starts:</strong>
                <input type="text" name="starts" class="form-control" value="{{ $event_info->starts }}">
            </div>
            <div class="form-group">
                <strong>Ends:</strong>
                <input type="text" name="ends" class="form-control" value="{{ $event_info->ends }}">
            </div>
            <div class="form-group">
                <strong>Image:</strong>
                <input type="text" name="image" class="form-control" value="{{ $event_info->image }}">
            </div>
            <div class="form-group">
                <strong>Description:</strong>
                <input type="text" name="description" class="form-control" placeholder="Description" value="{{ $event_info->description }}">
            </div>
            <div class="form-group">
                <strong>Organiser Name:</strong>
                <input type="text" name="organisername" class="form-control" placeholder="Organiser Name" value="{{ $event_info->organisername }}">
            </div>
            <div class="form-group">
                <strong>Organiser Description:</strong>
                <input type="text" name="organiserdescription" class="form-control" placeholder="Organiser Description" value="{{ $event_info->organiserdescription }}">
            </div>
            <div class="form-group">
                <strong>EventCategoryId:</strong>
                <select name="eventcategoryid" class="form-control" value="{{ $event_info->eventcategoryid }}">
                @foreach($eventcategory_info as $eventcategory_info)
                    <option value="{{ $eventcategory_info->id }}">{{ $eventcategory_info->id }}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <strong>EventTopicId:</strong>
                <select name="eventtopicid" class="form-control" value="{{ $event_info->eventtopicid }}">
                @foreach($eventtopic_info as $eventtopic_info)
                    <option value="{{ $eventtopic_info->id }}">{{ $eventtopic_info->id }}</option>
                @endforeach
                </select>           
            </div>
        </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection