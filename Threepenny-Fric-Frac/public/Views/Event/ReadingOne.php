<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <a href="/Event/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
                <i class="fas fa-edit"></i>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/Event/InsertingOne" class="tile">
                <i class="fas fa-plus"></i> 
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/Event/deleteOne/<?php echo $model['row']['Id'];?>" class="tile">
                <i class="fas fa-trash"></i>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/Event/Index" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam: </label>
                <span><?php echo $model['row']['Name']; ?></span>
            </div>
            <div>
                <label for="Name">Locatie: </label>
                <span><?php echo $model['row']['Location']; ?></span>
            </div>
            <div>
                <label for="Name">Start: </label>
                <span><?php echo $model['row']['Starts']; ?></span>
            </div>
            <div>
                <label for="Name">Einde: </label>
                <span><?php echo $model['row']['Ends']; ?></span>
            </div>
            <div>
                <label for="Name">Afbeelding: </label>
                <span><?php echo $model['row']['Image']; ?></span>
            </div>
            <div>
                <label for="Name">Beschrijving: </label>
                <span><?php echo $model['row']['Description']; ?></span>
            </div>
            <div>
                <label for="Name">Organisator Naam: </label>
                <span><?php echo $model['row']['OrganiserName']; ?></span>
            </div>
            <div>
                <label for="Name">Organisator Beschrijving: </label>
                <span><?php echo $model['row']['OrganiserDescription']; ?></span>
            </div>
            <div>
                <label for="Name">Event Categorie: </label>
                <span><?php echo $model['row']['EventCategoryId']; ?></span>
            </div>
            <div>
                <label for="Name">Event Topic: </label>
                <span><?php echo $model['row']['EventTopicId']; ?></span>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>