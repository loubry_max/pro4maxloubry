<section class="show-room entity">
    <form id="form" method="post" action="/Event/createOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <i class="fas fa-save"></i>
                <span class="screen-reader-text">Insert One</span>
            </button>
            <a href="/Event/Index" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam: </label>
                <input id="Name" name="Name" type="text" required />
            </div>
            <div>
                <label for="Location">Locatie: </label>
                <input id="Location" name="Location" type="text" required />
            </div>
            <div>
                <label for="Starts">Start: </label>
                <input id="Starts" name="Starts" type="datetime-local" />
            </div>
            <div>
                <label for="Ends">Einde: </label>
                <input id="Ends" name="Ends" type="datetime-local" />
            </div>
            <div>
                <label for="Image">Afbeelding: </label>
                <input id="Image" name="Image" type="text" required />
            </div>
            <div>
                <label for="Description">Beschrijving: </label>
                <input id="Description" name="Description" type="text"  required />
            </div>
            <div>
                <label for="OrganiserName">Organisator naam: </label>
                <input id="OrganiserName" name="OrganiserName" type="text"  required />
            </div>
            <div>
                <label for="OrganiserDescription">Organisator beschrijving: </label>
                <input id="OrganiserDescription" name="OrganiserDescription" type="text"  required />
            </div>
            <div>
                <label for="EventCategoryId">Event Category Id: </label>
                <select id="EventCategoryId" name="EventCategoryId">
                <?php
                foreach ($model['listEventCategory'] as $eventCategory) {
                    ?>
                    <option value="<?php echo $eventCategory['Id'] ?>"
                   >
                    <?php echo $eventCategory['Id'] ?></option>
                <?php } ?>
                </select>
            </div>
            <div>
                <label for="EventTopicId">Event Topic Id: </label>
                <select id="EventTopicId" name="EventTopicId">
                <?php
                foreach ($model['listEventTopic'] as $eventTopic) {
                ?>
                    <option value="<?php echo $eventTopic['Id'] ?>" 
                    >
                    <?php echo $eventTopic['Id'] ?></option>
                <?php } ?>
                </select>
            </div>
        </fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>
