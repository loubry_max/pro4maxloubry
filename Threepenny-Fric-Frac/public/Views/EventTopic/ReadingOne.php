<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventTopic</h2>
            <a href="/EventTopic/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
                <i class="fas fa-edit"></i>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/EventTopic/InsertingOne" class="tile">
                <i class="fas fa-plus"></i> 
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/EventTopic/deleteOne/<?php echo $model['row']['Id'];?>" class="tile">
                <i class="fas fa-trash"></i>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/EventTopic/Index" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam: </label>
                <span><?php echo $model['row']['Name']; ?></span>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>