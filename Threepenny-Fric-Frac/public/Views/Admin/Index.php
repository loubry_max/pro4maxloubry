    <h1><?php echo $model['title']; ?></h1>
    <article class="index">
        <a class="tile" href="/EventCategory/Index">
            <i class="fas fa-pencil-ruler"></i>
            <span class="screen-reader-text">Event Category</span>
            Event Category
        </a>
        <a class="tile" href="/Event/Index">
            <span aria-hidden="true" class="fas fa-calendar-check"></span>
            <span class="screen-reader-text">Event</span>
            Event 
        </a>
        <a class="tile" href="/EventTopic/Index">
            <span aria-hidden="true" class="fas fa-book-open"></span>
            <span class="screen-reader-text">Event Topic</span>
            Event Topic
        </a>    
        <div class="tile _2x1">Informatieve tegel</div>
</article>