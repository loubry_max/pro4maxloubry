<section class="show-room entity">
    <form id="form" method="post" action="/EventCategory/updateOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventCategory</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <i class="fas fa-save"></i>
                <span class="screen-reader-text">Update One</span>
            </button>
            <a href="/EventCategory/Index" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <input type="hidden" id="Id" name="Id" value="<?php echo $model['row']['Id'];?>" />
            <div>
                <label for="Name">Naam: </label>
                <input id="Name" name="Name" type="text" 
                    value="<?php echo $model['row']['Name'];?>" required />
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>