<?php

namespace Fricfrac\Controllers;

class EventController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['listEventCategory'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['listEventTopic'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            "Name" => $_POST['Name'],
            "Location" => $_POST['Location'],
            "Starts" => $_POST['Starts'],
            "Ends" => $_POST['Ends'],
            "Image" => $_POST['Image'],
            "Description" => $_POST['Description'],
            "OrganiserName" => $_POST['OrganiserName'],
            "OrganiserDescription" => $_POST['OrganiserDescription'],
            "EventCategoryId" => $_POST['EventCategoryId'],
            "EventTopicId" => $_POST['EventTopicId']
        );

        if (\AnOrmApart\Dal::create('Event', $event, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$event['Name']} is toegevoegd aan Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet toevoegen aan Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['listEventCategory'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['listEventTopic'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/Event/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['listEventCategory'] = \AnOrmApart\Dal::readAll('EventCategory', $Id);
        $model['listEventTopic'] = \AnOrmApart\Dal::readAll('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updateOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            'Id'=> $_POST['Id'],  
            'Name' => $_POST['Name'],
            "Location" => $_POST['Location'],
            "Starts" => $_POST['Starts'],
            "Ends" => $_POST['Ends'],
            "Image" => $_POST['Image'],
            "Description" => $_POST['Description'],
            "OrganiserName" => $_POST['OrganiserName'],
            "OrganiserDescription" => $_POST['OrganiserDescription'],
            "EventCategoryId" => $_POST['EventCategoryId'],
            "EventTopicId" => $_POST['EventTopicId']
        );

        if (\AnOrmApart\Dal::update('Event', $event, 'Name')) {
            $model['message'] = "Rij geupdated! {$event['Name']} is geupdated in Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet updaten uit Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    
    }

    public function deleteOne($Id)
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            "Id" => $Id
        );

        if (\AnOrmApart\Dal::delete('Event', $Id, 'Id')) {
            $model['message'] = "Rij gedeleted! {$Id} is gedeleted uit Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$Id} niet deleten uit Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }

}
