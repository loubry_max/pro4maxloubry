<?php


namespace Fricfrac\Controllers;

class EventCategoryController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventCategory',
            'error' => 'Geen'
        );
        $eventCategory = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventCategory', $eventCategory, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$eventCategory['Name']} is toegevoegd aan Event Category";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventCategory['Name']} niet toevoegen aan Event Category";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventCategory', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventCategory', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updateOne()
    {
        $model = array(
            'tableName' => 'EventCategory',
            'error' => 'Geen'
        );
        $eventCategory = array(
            'Id'=> $_POST['Id'],  
            'Name' => $_POST['Name']
        );

        if (\AnOrmApart\Dal::update('EventCategory', $eventCategory, 'Name')) {
            $model['message'] = "Rij geupdated! {$eventCategory['Name']} is geupdated in Event Category";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventCategory['Name']} niet updaten uit Event Category";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    
    }


    public function deleteOne($Id)
    {
        $model = array(
            'tableName' => 'EventCategory',
            'error' => 'Geen'
        );
        $eventCategory = array(
            "Id" => $Id
        );

        if (\AnOrmApart\Dal::delete('EventCategory', $Id, 'Id')) {
            $model['message'] = "Rij gedeleted! {$Id} is gedeleted uit Event Category";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$Id} niet deleten uit Event Category";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    }
}
