<?php


namespace Fricfrac\Controllers;

class EventTopicController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventTopic', $eventTopic, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$eventTopic['Name']} is toegevoegd aan Event Topic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventTopic['Name']} niet toevoegen aan Event Topic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function updateOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            'Id'=> $_POST['Id'],  
            'Name' => $_POST['Name']
        );

        if (\AnOrmApart\Dal::update('EventTopic', $eventTopic, 'Name')) {
            $model['message'] = "Rij geupdated! {$eventTopic['Name']} is geupdated in Event Topic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventTopic['Name']} niet updaten uit Event Topic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    
    }


    public function deleteOne($Id)
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            "Id" => $Id
        );

        if (\AnOrmApart\Dal::delete('EventTopic', $Id, 'Id')) {
            $model['message'] = "Rij gedeleted! {$Id} is gedeleted uit Event Topic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$Id} niet deleten uit Event Topic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }
}
