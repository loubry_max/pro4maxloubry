<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Je gerief klaarzetten</title>
</head>
<body>
    <h1>Programmeren 4</h1>
    <?php
        echo 'Hello PHP';
    ?>

    <?php
        /**
         * Created by JI.
         * Date: 24/02/2019
         * Time: 14:53
         * Voorbeeld hoe je echo gebruikt
         */
        echo '<p>laat uw humor, uw gloed, uw snelle boosaardige tong<br />';
        echo 'u vooral niet begeven<br />';
        echo 'en bezoek mij, eenmaal,<br />';
        echo 'in het huis van mijn ouderdom.</p>';
        echo '<address>Hendrik Marsman</address>'; 
    ?>
</body>
</html>