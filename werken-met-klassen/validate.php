<?php

namespace Helpers;

class Validate
{   
    public static function Regex($value, $regex){
        if (preg_match($regex,$value)) {
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public static function Filter($value, $filter, $errorMessage="Fout"){
        if (filter_var($value, $filter)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

?>