<?php

   define('REGEX_ULCHARONLY', '/^[a-zA-Z]* $/');
    
   function validateRegex($value, $regex){
    if (preg_match($regex,$value)) {
        return TRUE;
    }else{
        return FALSE;
    }

   }

   function validateFilter($value, $filter, $errorMessage="Fout"){
       if (filter_var($value, $filter)){
           return TRUE;
       }else{
           return FALSE;
       }
   }
?>