<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekenen</title>
</head>
<body>
    <p>
         <?php 
            define ('NEWLINE', '<br><hr/>');
            $i = 0;
            echo $i++;
            echo NEWLINE;
            echo ++$i;
            echo NEWLINE;
            $getal = 100;
            $huisnummer = '100';
            echo($getal == $huisnummer) ?  'waar' : 'onwaar';
            echo NEWLINE;
            echo($getal === $huisnummer) ?  'waar' : 'onwaar';
         ?>
    </p>
</body>
</html>