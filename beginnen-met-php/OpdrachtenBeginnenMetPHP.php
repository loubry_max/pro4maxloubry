<?php
    // OEFENINGEN: ARRAYS //

    // Oefening 1
    $color = array('white', 'green', 'red', 'blue', 'black');
    echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden sky. The new president and his first lady. - Richard M. Nixon";
?>
<?php
     // Oefening 2
    $color = array('white', 'green', 'red', 'blue', 'black');
    foreach ($color as $c)
    {
    echo "$c, ";
    }
    sort($color);
    echo "<ul>";
    foreach ($color as $y)
    {
    echo "<li>$y</li>";
    }
    echo "</ul>";
?>
<?php
    // Oefening 3
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg",
    "Belgium"=> "Brussels", "Denmark"=>"Copenhagen",
    "Finland"=>"Helsinki", "France" => "Paris",
    "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana",
    "Germany" => "Berlin", "Greece" => "Athens",
    "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam",
    "Portugal"=>"Lisbon", "Spain"=>"Madrid",
    "Sweden"=>"Stockholm", "United Kingdom"=>"London",
    "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius",
    "Czech Republic"=>"Prague", "Estonia"=>"Tallin",
    "Hungary"=>"Budapest", "Latvia"=>"Riga","Malta"=>"Valetta",
    "Austria" => "Vienna", "Poland"=>"Warsaw") ;
    asort($ceu) ;
    foreach($ceu as $country => $capital)
    {
    echo "The capital of $country is $capital"."\n" ;
    
    }
?>
<?php
     // Oefening 4
     $x = array(1, 2, 3, 4, 5);
     var_dump($x);
     unset($x[3]);
     $x = array_values($x);
     echo '
     ';
     var_dump($x);
?>    
<?php
     // Oefening 5
     $color = array(4 => 'white', 6 => 'green', 11=> 'red');
     echo reset($color)."\n";
?>
<?php 
    // Oefening 6
    function w3rfunction($value,$key)
    {
    echo "$key : $value"."\n";
    }
    $a = '{"Title": "The Cuckoos Calling",
    "Author": "Robert Galbraith",
    "Detail":
    { 
    "Publisher": "Little Brown"
    }
    }';
    $j1 = json_decode($a,true);
    array_walk_recursive($j1,"w3rfunction");
?>
<?php
    // Oefening 7
    $original = array( '1','2','3','4','5' );
    echo 'Original array : '."\n";
    foreach ($original as $x) 
    {echo "$x ";}
    $inserted = '$';
    array_splice( $original, 3, 0, $inserted ); 
    echo " \n After inserting '$' the array is : "."\n";
    foreach ($original as $x) 
    {echo "$x ";}
    echo "\n"
?>    
<?php
    // Oefening 8
    echo "
    Associative array : Ascending order sort by value
    ";
    $array2=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); asort($array2);
    foreach($array2 as $y=>$y_value)
    {
    echo "Age of ".$y." is : ".$y_value."
    ";
    }
    echo "
    Associative array : Ascending order sort by Key
    ";
    $array3=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); ksort($array3);
    foreach($array3 as $y=>$y_value)
    {
    echo "Age of ".$y." is : ".$y_value."
    ";
    }
    echo "
    Associative array : Descending order sorting by Value
    ";
    $age=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    arsort($age);
    foreach($age as $y=>$y_value)
    {
    echo "Age of ".$y." is : ".$y_value."
    ";
    }
    echo "
    Associative array : Descending order sorting by Key
    ";
    $array4=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); krsort($array4);
    foreach($array4 as $y=>$y_value)
    {
    echo "Age of ".$y." is : ".$y_value."
    ";
    } 
?>
<?php
    // Oefening 9
    $month_temp = "78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 81, 76, 73,
    68, 72, 73, 75, 65, 74, 63, 67, 65, 64, 68, 73, 75, 79, 73";
    $temp_array = explode(',', $month_temp);
    $tot_temp = 0;
    $temp_array_length = count($temp_array);
    foreach($temp_array as $temp)
    {
    $tot_temp += $temp;
    }
    $avg_high_temp = $tot_temp/$temp_array_length;
    echo "Average Temperature is : ".$avg_high_temp."
    "; 
    sort($temp_array);
    echo " List of five lowest temperatures :";
    for ($i=0; $i< 5; $i++)
    { 
    echo $temp_array[$i].", ";
    }
    echo "List of five highest temperatures :";
    for ($i=($temp_array_length-5); $i< ($temp_array_length); $i++)
    {
    echo $temp_array[$i].", ";
    }
?>   
<?php
    // Oefening 10
    function columns($uarr)
    {
    $n=$uarr;
    if (count($n) == 0)
    return array();
    else if (count($n) == 1)
    return array_chunk($n[0], 1);
    array_unshift($uarr, NULL);
    $transpose = call_user_func_array('array_map', $uarr);
    return array_map('array_filter', $transpose);
    }
    function bead_sort($uarr)
    {
    foreach ($uarr as $e)
    $poles []= array_fill(0, $e, 1);
    return array_map('count', columns(columns($poles)));
    }
    echo 'Original Array : '.'
    ';
    print_r(array(5,3,1,3,8,7,4,1,1,3));
    echo '
    '.'After Bead sort : '.'
    ';
    print_r(bead_sort(array(5,3,1,3,8,7,4,1,1,3)))
?>
<?php
    // Oefening 11
    $array1 = array(array(77, 87), array(23, 45));
    $array2 = array("w3resource", "com");
    function merge_arrays_by_index($x, $y)
    {
    $temp = array(); $temp[] = $x; if(is_scalar($y))
    {
    $temp[] = $y;
    }
    else
    {
    foreach($y as $k => $v)
    {
    $temp[] = $v;
    }
    }
    return $temp;
    }
    echo '<pre>'; print_r(array_map('merge_arrays_by_index',$array2, $array1));
?>   
<?php
    // Oefening 12
    function array_change_value_case($input, $ucase)
    {
    $case = $ucase;
    $narray = array();
    if (!is_array($input))
    {
    return $narray;
    }
    foreach ($input as $key => $value)
    {
    if (is_array($value))
    {
    $narray[$key] = array_change_value_case($value, $case);
    continue;
    }
    $narray[$key] = ($case == CASE_UPPER ? strtoupper($value) : strtolower($value));
    }
    return $narray;
    }
    $Color = array('A' => 'Blue', 'B' => 'Green', 'c' => 'Red');
    echo 'Actual array ';
    print_r($Color);
    echo 'Values are in lower case.';
    $myColor = array_change_value_case($Color,CASE_LOWER);
    print_r($myColor);
    echo 'Values are in upper case.';
    $myColor = array_change_value_case($Color,CASE_UPPER);
    print_r($myColor);
?>
<?php
    // Oefening 13
    echo implode(",",range(200,250,4))."\n";
?>
<?php
    // Oefening 14
    $my_array = array("abcd","abc","de","hjjj","g","wer");
    $new_array = array_map('strlen', $my_array);
    // Show maximum and minimum string length using max() function and min() function 
    echo "The shortest array length is " . min($new_array) .
    ". The longest array length is " . max($new_array).'.';
?>
<?php
    // Oefening 15
    $n=range(11,20);
    shuffle($n);
    for ($x=0; $x< 10; $x++)
    {
    echo $n[$x].' ';
    }
    echo "\n"
?>
<?php
    // Oefening 16
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels",
    "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava",
    "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin",
    "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm",
    "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=> "Valetta","Austria" => "Vienna", "Poland"=>"Warsaw") ;
    $max_key = max( array_keys( $ceu) ); 
    echo $max_key."\n";
?>
<?php
    // Oefening 17
    function min_values_not_zero(Array $values) 
    {
    return min(array_diff(array_map('intval', $values), array(0)));
    }
    print_r(min_values_not_zero(array(-1,0,1,12,-100,1))."\n");
?>
<?php
    // Oefening 18
    function floorDec($number, $precision, $separator)
    {
    $number_part=explode($separator, $number);
    $number_part[1]=substr_replace($number_part[1],$separator,$precision,0);
    if($number_part[0]>=0)
    {$number_part[1]=floor($number_part[1]);}
    else
    {$number_part[1]=ceil($number_part[1]);}

    $ceil_number= array($number_part[0],$number_part[1]);
    return implode($separator,$ceil_number);
    }
    print_r(floorDec(1.155, 2, ".")."\n");
    print_r(floorDec(100.25781, 4, ".")."\n");
    print_r(floorDec(-2.9636, 3, ".")."\n");
?>
<?php
    // Oefening 19
    $color = array ( "color" => array ( "a" => "Red", "b" => "Green", "c" => "White"),
    "numbers" => array ( 1, 2, 3, 4, 5, 6 ),
    "holes" => array ( "First", 5 => "Second", "Third"));
    echo $color["holes"][5]."\n"; // prints "second"
    echo $color["color"]["a"]."\n"; // prints "Red"
?>
<?php
    // OEFENINGEN: LOOP //

    // Oefening 1
    for($x=1; $x<=10; $x++)
    {
    if($x< 10)
    {
    echo "$x-";
    }
    else
    {
    echo "$x"."\n";
    }
    }
?>
<?php
    // Oefening 2
    $sum = 0;
    for($x=1; $x<=30; $x++)
    {
    $sum +=$x;
    }
    echo "The sum of the numbers 0 to 30 is $sum"."\n";
?>
<?php
    // Oefening 3
    for($x=1;$x<=5;$x++)
    {
    for ($y=1;$y<=$x;$y++)
        {
        echo "*";
            if($y< $x)
            {
            echo " ";
            }
        }
    echo "\n";
}
?>
<?php
    // Oefening 4
    $n=5;
    for($i=1; $i<=$n; $i++)
    {
    for($j=1; $j<=$i; $j++)
    {
    echo ' * ';
    }
    echo '\n';
    }
    for($i=$n; $i>=1; $i--)
    {
    for($j=1; $j<=$i; $j++)
    {
    echo ' * ';
    }
    echo '\n ';
    }
?>
<?php
    // Oefening 5
    $n = 6;
    $x = 1;
    for($i=1;$i<=$n-1;$i++)
    {
    $x*=($i+1); 
    }
    echo "The factorial of  $n = $x"."\n";
?>
<?php
    // Oefening 6
    for($a=0; $a< 10; $a++)
    { 
    for($b=0; $b< 10; $b++)
        {
            echo $a.$b.", "; 
        }
    }
    printf("\n"); 
?>
<?php
    // Oefening 7
    $text="w3resource";
    $search_char="r";
    $count="0";
    for($x="0"; $x< strlen($text); $x++)
    { 
        if(substr($text,$x,1)==$search_char)
        {
        $count=$count+1;
        }
    }
    echo $count."\n";
?>
<?php
    // Oefening 8
    $n = 5; 
    echo "n = " . $n . "\n";
    $count = 1;
    for ($i = $n; $i > 0; $i--) 
    {
    for ($j = $i; $j < $n + 1; $j++) 
    {
        printf("%4s", $count);
        $count++;
    } 
        echo "\n";
    }
?>
 <?php
 // Oefening 9
    for ($row=0; $row<7; $row++)
    {
    for ($column=0; $column<=7; $column++)
        {
        if ($column == 1 or (($row == 0 or $row == 3 or $row == 6) and ($column < 5 and $column > 1)) or ($column == 5 and ($row != 0 and $row != 3 and $row != 6)))
                echo "*";    
            else  
                echo " ";     
        }        
    echo "\n";
    }
?>
<?php
    // OEFENINGEN: FUNCTIONS //

    // Oefening 1
    function factorial_of_a_number($n)
    {
    if($n ==0)
        {
        return 1;
        }
    else 
        {	
        return $n * factorial_of_a_number($n-1);
        }
        }
    print_r(factorial_of_a_number(4)."\n");
?>
<?php
    // Oefening 2
    function IsPrime($n)
    {
    for($x=2; $x<$n; $x++)
    {
        if($n %$x ==0)
            {
            return 0;
            }
        }
    return 1;
    }
    $a = IsPrime(3);
    if ($a==0)
    echo 'This is not a Prime Number.....'."\n";
    else
    echo 'This is a Prime Number..'."\n";
?>
<?php
    // Oefening 3
    function reverse_string($str1)
    {
    $n = strlen($str1);
    if($n == 1)
    {
        return $str1;
    }
    else
    {
    $n--;
    return reverse_string(substr($str1,1, $n)) . substr($str1, 0, 1);
    }
    }
    print_r(reverse_string('1234')."\n");
?>
<?php
    // Oefening 4
    function array_sort($a)
    {
    for($x=0;$x< count($a);++$x)
    {
        $min = $x;
    for($y=$x+1;$y< count($a);++$y)
    {
        if($a[$y] < $a[$min] ) 
        {
        $temp = $a[$min];
        $a[$min] = $a[$y];
        $a[$y] = $temp;
        }
        }
    }
    return $a;
    }
    $a = array(51,14,1,21,'hj');
    print_r(array_sort($a));
?>
<?php
    // Oefening 5
    function is_str_lowercase($str1)
    {
        for ($sc = 0; $sc < strlen($str1); $sc++) {
            if (ord($str1[$sc]) >= ord('A') &&
            ord($str1[$sc]) <= ord('Z')) {
        return false;
            }
            }
        return true;
        }
    var_dump(is_str_lowercase('abc def ghi'));
    var_dump(is_str_lowercase('abc dEf ghi'));
?>
<?php
    // Oefening 6
    function check_palindrome($string) 
    {
    if ($string == strrev($string))
        return 1;
    else
        return 0;
    }
    echo check_palindrome('madam')."\n";
?>