<?php
    $voornaam = 'Sarah';
    $familienaam = 'Costers';
    $postcode = '2020';
    $jaar = 2020;
    $zip = '6520';
	$month = 2;
	$day = 6;
    $year = 2018;
    $price = 5;
    $tax = 12;
	
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Werken met tekst</title>
</head>
<body>
    <h1> 
        <?php echo "Je voornaam is $voornaam en je familienaam is $familienaam";

        ?>
    </h1>
    <h2><?php echo 'Je voornaam is ' . $voornaam .  ' en je familienaam is ' . $familienaam;
    
    ?></h2>
    <p>
        <?php
        echo( $jaar === $postcode ) 
            ? "Het jaar $jaar is gelijk aan $postcode"
            : "Het jaar $jaar is niet gelijk aan $postcode";
        ?>
    </p>
    <p> <?php
        echo sprintf('De maaltijd kost $%.2f', $price  +  ($price/100 * $tax));
        ?>
    </p>
    <p> <?php
    echo sprintf("Postcode is %05d en de datum is %02d/%02d/%d", $zip, $month, $day, $year);
    ?>
    </p>
</body>
</html>