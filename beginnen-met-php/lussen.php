<?php
    // een array met 100 elementen
    $postcodes = array();
    for($i = 1; $i <=100; $i++ ) {
    $postcodes[] = (string) rand(1, 9999);

    }
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lussen</title>
</head>
<body>
<form action="">
    <div><label for="postalCode">Postcodes</label>
    <select id="postalCode">
            <?php
                for ($i = 0; $i <= count($postcodes) - 1; $i++) {
            ?>
            <option><?php echo $postcodes[$i];?></option>
            <?php
                }
            ?>
</form>    

</body>
</html>