<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/form.css">
    <title>Formulier</title>
</head>

<body>
    <div id="page">
        <p>Vul het registratieformulier in. Velden met een * zijn verplicht.</p>
        <form method="POST" action="opdracht.php"> 
            <fieldset class='first'>
                <legend>Accountgegevens</legend>
                <div>
                    <label for="firstname">Voornaam *</label>
                    <input id="firstname" type="text" name="firstname" required="required">
                    <?php if(isset($uFirstName_error)) { ?>
                        <p><?php echo $uFirstName_error ?></p>
                    <?php } ?>
                    <span class="error"></span>
                </div>
                <div>
                    <label for="lastname">Familienaam *</label>
                    <input id="lastname" type="text" name="lastname" required="required">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="email">E-mail *</label>
                    <input id="email"  type="email" name="email" required="required" placeholder="name@example.com">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="password1">Wachtwoord *</label>
                    <input id="password1" type="password" name="password1" maxlength="20" required="required">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="password2">Bevestig wachtwoord *</label>
                    <input id="password2" type="password" name="password2" maxlength="20" required="required">
                    <span class="error"></span>
                </div>
            </fieldset>
            <fieldset class='first'>
                <legend>Adresgegevens</legend>
                <div>
                    <label for="adress1">Adres 1 *</label>
                    <input id="adress1" type="text" name="adress1" required="required">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="adress2">Adres 2</label>
                    <input id="adress2" type="text" name="adress2">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="postalcode">Postcode *</label>
                    <input id="postalcode" type="number" name="postalcode"  required="required">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="city">Stad *</label>
                    <input id="city" type="text" name="city" required="required">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="countryCode">Land</label>
                    <select name="countryCode" id="countryCode">
                        <option value="BE">België</option>
                        <option value="FR">Frankrijk</option>
                        <option value="NL">Nederland</option>
                        <option value="DE">Duitsland</option>
                    </select>
                </div>
            </fieldset>
            <fieldset id="second">
                <legend>Persoonlijke gegevens</legend>
                <div>
                    <label for="phone">GSM</label>
                    <input id="phone" type="text" name="phone">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="birthday">Geboortedatum </label>
                    <input id="birthday" type="date" name="birthday">
                    <span class="error"></span>
                </div>
                <div>
                    <label for="satisfied">Hoe tevreden ben je?</label>
                    <input id="satisfied" name="satisfied" type="range" min="-4" max="2" step="2" value="-2"/>
                    <span class="error"></span>
                </div>
                <div>
                    <label class='floatl' for="male">Man</label>
                    <input id="male" class='nomargin' type="radio" name="sex-male" value="man"><br>
                    <span class="error"></span>
                    <label class='floatl' for="female">Vrouw</label>
                    <input id ="female" class='nomargin' type="radio" name="sex-female" value="vrouw"><br>
                    <label class='floatl' for="unknown">Onbekend</label>
                    <input id ="uknown" class='nomargin' type="radio" name="sex-uknown" value="uknown"><br>
                    <span class="error"></span>
                </div>
                <div>
                    <label for="faculty">Kies een afdeling</label>
                    <select name="faculty" id="faculty">
                        <option value="HBO-Informatica">HBO Informatica</option>
                        <option value="HBO-Marketing">HBO Marketing</option>
                    </select>
                </div>
                <div>
                    <label for="modules">Kies een module</label>
                    <select id="modules" name="modules">
                        <option value="Multimedia">Multimedia</option>
                        <option value="Databanken">Databanken</option>
                        <option value="Programmeren3">Programmeren 3</option>
                        <option value="Programmeren3">Programmeren 4</option>
                    </select>
                </div>
            </fieldset> 
            <fieldset id="full">
                <div>
                    <button id="submit" type="submit" value="Verzenden" name="submit" > Verzenden </button>
                    <span class="error"></span>
                </div>
            </fieldset>
        </form>
    </div>
</body>

</html>