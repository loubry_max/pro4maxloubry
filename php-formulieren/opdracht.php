<?php

    $uFirstName = $_POST['firstname'];
    $uLastName = $_POST['lastname'];
    $uEmail = $_POST['email'];
    $uPassword1 = $_POST['password1'];
    $uPassword2 = $_POST['password2'];
    $uAdress1 = $_POST['adress1'];
    $uCity = $_POST['city'];
    $uPostalCode = $_POST['postalcode'];
    $uCountryCode = $_POST['countryCode'];

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty($uFirstName)){
            $uFirstName_error = "Geef uw voornaam mee";
        }
        
        if(empty($uLastName)){
            $uLastName_error = "Geef uw Familienaam mee";
        }
   
        if(empty($Email)){
            $uEmail_error = "Geef uw Email-adres mee";
        }
        
        if(empty($uPassword1)){
            $uPassword1_error = "Voer een wachtwoord in";
        } elseif(strlen($uPassword1) < 8){
            $uPassword1_error= "Uw wachtwoord moet minstens 8 karakters bevatten";
        }
        
        if(!preg_match("/^[a-zA-Z ]*$/",$uPassword1)){
            $uPassword1_error = "Uw wachtwoord moet minstens één hoofdletter en een speciaal teken hebben";
        }

        if($uPassword1 == $uPassword2){

        }else{
            $uPassword2_error = "De wachtwoorden moeten overeenkomen";
        }
        
    }
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <p>Dit heeft u ingevuld!</p>
<article>
    <ol>
        <li>Voornaam:
            <?php echo isset($_POST['firstname']) ? $_POST['firstname'] : 'Voornaam niet opgegeven!'; ?>
        </li>
        <li>Familienaam:
            <?php echo isset($_POST['lastname']) ? $_POST['lastname'] : 'Familienaam niet opgegeven!'; ?>
        </li>
        <li>E-mail adres:
            <?php echo isset($_POST['email']) ? $_POST['email'] : 'E-mailadres niet opgegeven!'; ?>
        </li>
        <li>Wachtwoord:
            <?php echo isset($_POST['password1']) ? $_POST['password1'] : 'Wachtwoord niet opgegeven!'; ?>
        </li>
        <li>Adres 1:
            <?php echo isset($_POST['adress1']) ? $_POST['adress1'] : 'Adres niet opgegeven!'; ?>
        </li>
        <li>Adres 2:
            <?php echo isset($_POST['adress2']) ? $_POST['adress2'] : 'Adres niet opgegeven!'; ?>
        </li>
        <li>Postcode:
            <?php echo isset($_POST['postalcode']) ? $_POST['postalcode'] : 'Postcode niet opgegeven!'; ?>
        </li>
        <li>Stad:
            <?php echo isset($_POST['city']) ? $_POST['city'] : 'Stad niet opgegeven!'; ?>
        </li>
        <li>Land:
            <?php echo isset($_POST['countryCode']) ? $_POST['countryCode'] : 'Land niet opgegeven!'; ?>
        </li>
        <li>Telefoonnummer:
            <?php echo isset($_POST['phone']) ? $_POST['phone'] : 'Telefoonnummer niet opgegeven!'; ?>
        </li>
        <li>Geboortedatum:
            <?php echo isset($_POST['birthday']) ? $_POST['birthday'] : 'Geboortedatum niet opgegeven!'; ?>
        </li>
        <li>Hoe tevreden ben je?:
            <?php echo isset($_POST['satisfied']) ? $_POST['satisfied'] : 'Tevredenheid niet opgegeven!'; ?>
        </li>
        <li>Wat is je geslacht?
            <ol>
                <li>
                    <?php echo isset($_POST['sex-male']) ? $_POST['sex-male'] : 'Man niet aangevinkt!'; ?>
                </li>
                <li>
                    <?php echo isset($_POST['sex-female']) ? $_POST['sex-female'] : 'vrouw niet aangevinkt!'; ?>
                </li>
                <li>
                    <?php echo isset($_POST['sex-unknown']) ? $_POST['sex-unknown'] : 'onbekend niet aangevinkt!'; ?>
                </li>
            </ol>
        </li>
        <li>Afdeling:
            <?php echo isset($_POST['faculty']) ? $_POST['faculty'] : 'Afdeling niet aangevinkt!'; ?>
        </li>
        <li>Module:
            <?php echo isset($_POST['modules']) ? $_POST['modules'] : 'Module niet aangevinkt!'; ?>
        </li>
        
    </ol> 
</article>
</body>
</html>