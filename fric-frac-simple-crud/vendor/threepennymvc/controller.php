<?php

namespace ThreepennyMVC;

class Controller
{
    public function view($model = null, $path = null)
    {
        if (!isset($path)) {

            $folderName =  str_replace('Controller', '', (new \ReflectionClass($this))->getShortName());
            $fileName = ucfirst(debug_backtrace()[1]['function']);
            $path = 'Views' . DIRECTORY_SEPARATOR . $folderName . DIRECTORY_SEPARATOR . $fileName . '.php';
        }
        $view = function () use ($model, $path) {
            include($path);
        };
        return $view;
    }
}
