<?php
// var_dump($_SERVER);
use ThreepennyMVC\FrontController;
include ('../vendor/threepennymvc/FrontController.php');
include ('../vendor/threepennymvc/Controller.php');
include ('../vendor/anormapart/Dal.php');
include ('../vendor/anormapart/Helpers.php');
include ('Controllers/AdminController.php');
include ('Controllers/EventController.php');
$route = FrontController::getRouteData($_SERVER['REQUEST_URI'], 'Fricfrac', 'Admin', 'index');
$view = FrontController::dispatch($route);
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/icon-font.css">
    <script src="https://kit.fontawesome.com/883419c36b.js" crossorigin="anonymous"></script>
    <title>Fric-frac events</title>
</head>
<body class="page">
<header class="page-header">
        <nav class="control-panel">
            <a href="/index.php" class="tile">
                <i class="fas fa-bars"></i>
                <span class="screen-reader-text">Admin index</span>
            </a>
        </nav>
        <h1 class="banner">Fric-frac</h1>
    </header>
    <?php echo $view();?>
    <footer class="page-footer">
        <p>&copy ModernWays 2020</p>
        <p>Opdracht Programmeren 4</p>
    </footer>
</body>
</html>