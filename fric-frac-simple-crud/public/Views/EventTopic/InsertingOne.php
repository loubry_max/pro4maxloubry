<section class="show-room entity">
    <form id="form" method="post" action="/EventTopic/createOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventTopic</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <i class="fas fa-save"></i>
                <span class="screen-reader-text">Insert One</span>
            </button>
            <a href="/EventTopic/Index" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam</label>
                <input type="text" required id="Name" name="Name" />
            </div>
        </fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>