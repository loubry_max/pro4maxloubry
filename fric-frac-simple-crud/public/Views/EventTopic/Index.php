<section class="show-room entity">
    <div class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventTopic</h2>
            <a href="/EventTopic/InsertingOne.php" class="tile">
                <i class="fas fa-plus"></i> 
                <span class="screen-reader-text">InsertingOne</span>
            </a>
        </nav>
        <fieldset></fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </div>
    <?php include('ReadingAll.php'); ?>
</section>