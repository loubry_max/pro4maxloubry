    <h1><?php echo $model['title']; ?></h1>
    <article class="index">
        <a class="tile" href="/EventCategory/Index.php">
            <i class="fas fa-pencil-ruler"></i>
            <span class="screen-reader-text">Event Category</span>
            Event Category
        </a>
        <a class="tile" href="EventTopic/Index.php">
            <span aria-hidden="true" class="fas fa-book-open"></span>
            <span class="screen-reader-text">Event Topic</span>
            Event Topic</a>
        <a class="tile" href="Event/Index.php">
            <span aria-hidden="true" class="fas fa-calendar-check"></span>
            <span class="screen-reader-text">Event</span>
            Event Index</a>
        <div class="tile _2x1">Informatieve tegel</div>
</article>