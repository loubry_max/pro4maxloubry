<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventCategorie</h2>
            <a href="/EventCategory/UpdatingOne/{placeholder}" class="tile">
                <i class="fas fa-edit"></i>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/EventCategory/InsertingOne" class="tile">
                <i class="fas fa-plus"></i> 
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/EventCategory/deleteOne/{placeholder}" class="tile">
                <i class="fas fa-trash"></i>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/EventCategory/Index.php" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam</label>
                <span>{placeholder}</span>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>