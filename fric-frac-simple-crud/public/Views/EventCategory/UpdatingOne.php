<header class="page-header">
    <nav class="control-panel">
        <a href="/index.php" class="tile">
            <i class="fas fa-bars"></i>
            <span class="screen-reader-text">Admin index</span>
        </a>
    </nav>
    <h1 class="banner">Fric-frac</h1>
</header>
<section class="show-room entity">
    <form id="form" method="post" action="/EventCategory/updateOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventCategorie</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <i class="fas fa-save"></i>
                <span class="screen-reader-text">Update One</span>
            </button>
            <a href="/EventCategory/Index.php" class="tile">
                <i class="fas fa-times"></i>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <input type="hidden" id="Id" name="Id" value="{placeholder}" />
            <div>
                <label for="Name">Naam</label>
                <input id="Name" name="Name" type="text" value="{placeholder}" required />
            </div>
        </fieldset>
        <div class="feedback">
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>