<?php
function cleanUpFileName ($requestUri){
   $queryStart = strpos($requestUri, '?');
   if ($queryStart > 0){
      $requestUri = substr($requestUri, 0, $queryStart);
   }
   return $requestUri;
}
$uri = $_SERVER['REQUEST_URI'];
$uri = cleanUpFileName($uri);
if(file_exists(__DIR__ . '/' . $uri)){
   return false;
}
else{
   include_once 'index.php';
}